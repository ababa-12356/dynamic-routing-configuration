import request from "@/utils/request";
/**
 * 描述 登录函数
 * @author kkdy
 * @date 2021-10-28
 * @param {any} data
 * @returns {any}
 */
export function login(data) {
  return request({
    url: "/api/login",
    method: "post",
    data,
  });
}
/**
 * 描述 获取用户信息
 * @author kkdy
 * @date 2021-10-28
 * @param {any} data
 * @returns {any}
 */
export function getUserInfo(data) {
  return request({
    method: "get",
    url: "api/getUserInfo",
    data,
  });
}
/**
 * 描述 用户登出
 * @author kkdy
 * @date 2021-10-28
 * @param {any} data
 * @returns {any}
 */
export function logout(data) {
  return request({
    method: "get",
    url: "/logout",
    data,
  });
}
