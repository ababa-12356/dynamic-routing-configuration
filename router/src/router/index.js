import Vue from "vue";
import Vuex from "vuex";
import VueRouter from "vue-router";
import Login from "@/views/login";
import store from "@/store/index";
import { getUserInfo } from "@/api/login.js";
Vue.use(VueRouter);
Vue.use(Vuex);

const routes = [
  {
    path: "/",
    name: "home",
    component: () => import("@/views/home.vue"),
  },
  // 前台登录页
  {
    path: "/Login",
    name: "Login",
    component: Login,
  },
  {
    path: "/translation",
    component: () => import("@/views/translation/vue-translation.vue"),
  },
  {
    path: "/vuex",
    component: () => import("@/views/vuex/index.vue"),
  },
];
const router = new VueRouter({
  routes,
  store,
});
router.beforeEach((to, form, next) => {
  //判断Vuex中是否有参数缓存
  if (store && store.state.navSide.length === 0) {
    // 发送请求 获取该用户对应的菜单列表
    getUserInfo().then((res) => {
      if (res.code === 1) {
        res = [
          {
            title: "饿了吗",
            icon: "el-icon-eleme",
            path: "x",
            child: [
              {
                title: "阶段1-1",
                icon: "el-icon-price-tag",
                path: "1-1",
              },
              {
                title: "阶段1-2",
                icon: "el-icon-price-tag",
                path: "1-2",
              },
            ],
          },
          {
            title: "设置",
            icon: "el-icon-s-tools",
            path: "y",
            child: [
              {
                title: "阶段2-1",
                icon: "el-icon-price-tag",
                path: "2-1",
              },
              {
                title: "阶段2-2",
                icon: "el-icon-price-tag",
                path: "2-2",
              },
            ],
          },
          {
            title: "帮助",
            icon: "el-icon-s-help",
            path: "3",
          },
        ];
        store.dispatch("setNav", res);
        next();
      }
    });
  }
});
export default router;
