import Cookies from "js-cookie";
function getToken() {
  let v = Cookies.get("token");
  if (v) {
    return v;
  } else {
    return null;
  }
}
export default getToken;
