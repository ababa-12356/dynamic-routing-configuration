export default [
  {
    // 管理员登录后的欢迎页
    path: "/admin",
    component: () => import("./index.vue"),
  },
];
