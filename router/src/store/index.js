import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    count: 0,
    userInfo: {},
    navSide: [],
  },
  getters: {
    getCount(state) {
      return state.count;
    },
    getUserRole(state) {
      return state.userInfo.roleName;
    },
  },
  mutations: {
    addCount(state, num) {
      state.count = state.count + num;
    },
    oddCount(state, num) {
      state.count = state.count - num;
    },
    loginByToken(state, userInfo) {
      state.userInfo = userInfo;
    },
    // 存储侧边栏菜单
    getNav(state, nav) {
      state.navSide = nav;
    },
  },
  actions: {
    addCount(context, e) {
      if (e === 1) {
        context.commit("addCount", e);
      } else {
        context.commit("oddCount", e);
      }
    },
    // 存储侧边栏菜单
    setNav({ commit }, data) {
      commit("getNav", data);
    },
  },
  modules: {},
});
