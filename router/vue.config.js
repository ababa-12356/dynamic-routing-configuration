// const path = require('path');

// const webpack = require('webpack')
// const CompressionWebpackPlugin = require('compression-webpack-plugin')
// const productionGzipExtensions = ['js', 'css']
// const isProduction = process.env.NODE_ENV === 'production'
//配置webpack打包时的参数
module.exports = {
  productionSourceMap: false,
  //所有资源使用相对路径
  publicPath: "./",
  //打包后资源存放的目录
  outputDir: "dist",
  //设置所有标签属性有双引号
  chainWebpack: (config) => {
    config.plugin("html").tap((args) => {
      args[0].minify = false;
      return args;
    });
    // 移除 preload 插件
    config.plugins.delete("preload");
    // 移除 prefetch 插件
    config.plugins.delete("prefetch");
  },
  // 请求代理配置
  devServer: {
    open: true,
    host: "localhost",
    port: 8080,
    https: false,
    //以上的ip和端口是我们本机的
    proxy: {
      "/api": {
        //需要跨域的地址
        target: "http://192.168.0.116:9000",
        ws: true,
        //允许跨域
        changOrigin: true,
        pathRewrite: {
          //请求的时候使用这个api就可以
          "^/api": "",
        },
      },
    },
  },
  // configureWebpack: {
  //   resolve: {
  //     alias: {
  //       '@': path.resolve(__dirname, './src'),
  //       '@i': path.resolve(__dirname, './src/assets'),
  //     }
  //   },
  //   plugins: [
  //     new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),

  //     // 下面是下载的插件的配置
  //     new CompressionWebpackPlugin({
  //       algorithm: 'gzip',
  //       test: new RegExp('\\.(' + productionGzipExtensions.join('|') + ')$'),
  //       threshold: 10240,
  //       minRatio: 0.8
  //     }),
  //     new webpack.optimize.LimitChunkCountPlugin({
  //       maxChunks: 5,
  //       minChunkSize: 100
  //     })
  //   ]
  // }
};
